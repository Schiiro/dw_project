/*
 * File:   wifi.c
 * Author: doria
 *
 * Created on 14 mars 2020, 10:18
 */


#include "header.h"

extern char TAB_WIFI[255]; //Tableau RFID


void Wifi_Init(void) {
    
    WIFITX_SENS = OUTPUT; //Sortie
    WIFIRX_SENS = INPUT; //Entree
    
    __builtin_write_OSCCONL(OSCCON & 0xBF);
    
    RPINR18bits.U1RXR = 19; //pin RX
    RPOR13bits.RP27R = 3;  //pin TX
    
    __builtin_write_OSCCONL(OSCCON | 0x40);
    
    //Configuration BAUD
    U1MODEbits.BRGH = 1;
    long temp;    
    temp = FCY;
    temp /= 4;
    temp /= 115200L; // baud_rate
    temp--;
    U1BRG = temp; //
    __delay_ms(100);
    
    U1MODEbits.UARTEN = 1;
    U1STAbits.UTXEN = 1;
    __delay_ms(100);
    
}


void Wifi_Write(char _letter)
{
    
    while(U1STAbits.UTXBF == 1); //Wait canal libre
    
    U1TXREG = _letter; //envoi d'un char
       
}

void Wifi_Phrase(char* _phrase)
{
    while(*_phrase)
        Wifi_Write(*_phrase++);
    
}


void Wifi_Reset()
{
    
    Wifi_Phrase("AT+RST");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
       
}

void Wifi_GetList()
{
    Wifi_Phrase("AT+CWLAP");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
}

void Wifi_Mode()
{
    Wifi_Phrase("AT+CWMODE=1");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
}

void Wifi_Connect()
{
    Wifi_Phrase("AT+CWJAP=\"");
    Wifi_Phrase("Freebox_GEGAVJ");
    Wifi_Phrase("\",\"");
    Wifi_Phrase("ConnexionSecuriteWPA");
    Wifi_Phrase("\"");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
}

void Wifi_GetIP()
{
    Wifi_Phrase("AT+CIFSR");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
}

void Wifi_Mux()
{
    Wifi_Phrase("AT+CIPMUX=1");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
}

void Wifi_Server()
{
    Wifi_Phrase("AT+CIPSERVER=1,80");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
}



void Wifi_Setup()
{
    Wifi_Reset();
    __delay_ms(3000);
    Wifi_Mode();
    __delay_ms(3000);
    Wifi_GetList();
    __delay_ms(7000);
    Wifi_Connect();
    __delay_ms(12000);
    Wifi_GetIP();
    __delay_ms(700);
    Wifi_Mux();
    __delay_ms(700);
    Wifi_Server();
    __delay_ms(700);
}

void Wifi_Send_test()
{
    Wifi_Phrase("AT+CIPSEND=0,13");
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
    __delay_ms(1000);
    Wifi_Phrase("DeeWeeTest!");
    /*int i = 0;
    char test[255];
    for(i; i < 255; i++)
    {
        test[i] =  TAB_WIFI[i];
        Wifi_Write(test[i]);
    }*/
    Wifi_Write(0x0D);
    Wifi_Write(0x0A);
    __delay_ms(1000);
    //Wifi_Phrase("AT+CIPCLOSE=0");
}

