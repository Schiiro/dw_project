#include "header.h"

void Nfc_Init(void) {
    
    NFCTX_SENS = OUTPUT; //Sortie
    NFCRX_SENS = INPUT; //Entree
    
    __builtin_write_OSCCONL(OSCCON & 0xBF);
    
    RPINR19bits.U2RXR = 8; //pin RX
    RPOR6bits.RP13R = 5;  //pin TX
    
    __builtin_write_OSCCONL(OSCCON | 0x40);
    
    //Configuration BAUD
    U2MODEbits.BRGH = 1;
    long temp;    
    temp = FCY;
    temp /= 4;
    temp /= 115200; // baud_rate
    temp--;
    U2BRG = temp; //
    __delay_ms(100);
    
    U2MODEbits.UARTEN = 1;
    U2STAbits.UTXEN = 1;
    __delay_ms(100);
    
}


void Nfc_Write(char _letter)
{
    
    while(U2STAbits.UTXBF == 1); //Wait canal libre
    
    U2TXREG = _letter; //envoi d'un char
       
}

void Nfc_Phrase(char* _phrase)
{
    while(*_phrase)
        Wifi_Write(*_phrase++);
    
}
