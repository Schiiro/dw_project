/* 
 * File:   header.h
 * Author: doria
 *
 * Created on 21 mars 2020, 23:55
 */

#ifndef HEADER_H
#define	HEADER_H

//Clock System
#define SYS_FREQ    32000000L  //Horloge PIC
#define FCY         SYS_FREQ/2 //CPU CLOCK

//All include
#include <libpic30.h>
#include "xc.h"
#include <stdio.h>
#include <stdlib.h>

//General Define
#define HIGH 1
#define LOW 0
#define INPUT 1
#define OUTPUT 0
#define NUMERIC 1
#define ANALOGIC 0

#define SSID Freebox_GEGAVJ
#define PSW ConnexionSecuriteWPA

//LED
#define LED_SENS TRISBbits.TRISB7
#define LED PORTBbits.RB7

void Led_Init(void);
void Led_On();
void Led_Off();


//BUZZER
#define BUZ_SENS TRISBbits.TRISB11
#define BUZ PORTBbits.RB11

void Buz_Init(void);
void Buz_On();
void Buz_Off();


//WIFI
#define WIFITX_SENS TRISGbits.TRISG9
#define WIFIRX_SENS TRISGbits.TRISG8

void Wifi_Init(void);
void Wifi_Write(char _letter);
void Wifi_Phrase(char* _phrase);
void Wifi_Reset();
void Wifi_GetList();
void Wifi_Mode();
void Wifi_Connect();
void Wifi_GetIP();
void Wifi_Setup();
void Wifi_Mux();
void Wifi_Server();
void Wifi_Send_test();


//NFC
#define NFCTX_SENS TRISBbits.TRISB2
#define NFCRX_SENS TRISBbits.TRISB8

void Nfc_Init(void);
void Nfc_Write(char _letter);


//PAVE NUMERIC
/*#define
#define
#define
#define
#define
#define
#define
#define


*/
#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* HEADER_H */

