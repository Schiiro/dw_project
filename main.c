/* 
 * File:   main.c
 * Author: doria
 *
 * Created on 21 mars 2020, 23:53
 */

#include "header.h"

/*
 * 
 */

char TAB_NFC[20]; //Tableau RFID
char INDEX_TAB_NFC = 0; //Curseur 

char TAB_WIFI[255]; //Tableau RFID
char INDEX_TAB_WIFI = 0; //Curseur 


int main(void) {
    
    //INIT
    __delay_ms(1000);
    int j = 0;
    for(j; j < 255; j++)
    {
         TAB_WIFI[j] = 'T';
    }
    
    Led_Init();
    Wifi_Init();
    Wifi_Setup();
    Nfc_Init();
    Buz_Init();
    Led_On();
    Buz_On();    
    Buz_Off();
    Led_Off();
    __delay_ms(5000);
   
    
    //RUN
    while(1)
    {
        Wifi_Send_test();
        __delay_ms(5000);
    }    

    return (EXIT_SUCCESS);
}

