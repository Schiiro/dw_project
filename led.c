/*
 * File:   led.c
 * Author: doria
 *
 * Created on 14 mars 2020, 10:18
 */



#include "header.h"

void Led_Init(void) {
    LED_SENS = OUTPUT; //Pin en sortie   
    Led_Off();
}

void Led_On() {
    LED = HIGH;
}

void Led_Off() {
    LED = LOW;
}

